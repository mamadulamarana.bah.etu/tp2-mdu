#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <assert.h>
#include <dirent.h>
#include <string.h>
#include <limits.h>
#include <search.h>

/* Definition des variables globales */
int opt_follow_links = 0;
int opt_apparent_size = 0;


/* Exercice 1 */
int du_single_file(const char *pathname)
{
    struct stat buff;
    long int size = 0;
    int state;
    if (opt_follow_links == 0) {
        state = lstat(pathname, &buff);
    }
    if (opt_follow_links == 1) {
        state = stat(pathname, &buff);
    }
    if (state < 0) {
        printf("erreur sur stat1\n");
        return -1;
    }

    if (opt_apparent_size == 1){
        size = buff.st_size;
    }
    if (opt_apparent_size == 0) {
        size = buff.st_blocks;
    }
    return size;    
}

/* Exercice 2 */
int is_dir(const char *pathname)
{
    struct stat buf;
    int state = stat(pathname, &buf);
    if (state < 0) {
        printf("erreur sur stat2\n");
        return -1;
    }

    if ( S_ISDIR(buf.st_mode) ) {
        return 1;
    }
    return 0;
}


/* Exercice 3 */
int du_file(const char *pathname)
{
    long int res = 0;

    if (is_dir(pathname) == 0) {
        return du_single_file(pathname);
    }

    else if( is_dir(pathname) == 1 ) {
        DIR *dir = opendir(pathname);
        if (dir == NULL) {return -1;};

        struct dirent *dirp;
        dirp = readdir(dir);
        while (dirp != NULL) {
            char *file = dirp->d_name;
            if ( strcmp(file, ".") == 0 || strcmp(file, "..") == 0) {
            }

            else {
                char path[256];
                strcpy(path, pathname);
                strcat(path, "/");
                strcat(path, file);
                res += du_file(path);
            }
            dirp = readdir(dir);
        }
        closedir(dir);
    }
    return res;
}

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "answers.h"

int main(int argc, char *argv[])
{
    int opt;

    /* Pour les exercices 4 et 5, il faut processer les option -b et
       -L.  Pour le faire, regarder la fonction getopt():

       man 2 getopt
    */

    if (argc < 2) {
        printf("At least one argument is required\n");
        exit(-1);
    }
    while( (opt = getopt(argc, argv, "bL")) != -1) {
        switch (opt) {
            case 'b':
                opt_apparent_size = 1;
                break;
            case 'L':
                opt_follow_links = 1;
                break;
            default:
                opt_apparent_size = 0;
                opt_follow_links = 0;
        }
    }

    
    printf("The options are: opt_apparent_size = %d, opt_follow_links = %d\n",
           opt_apparent_size, opt_follow_links);
    int s;
    int n = 0;
    if (argc > 2) {
        s = du_file(argv[2]);
        n = 2;
    }
    else {
        s = du_file(argv[1]);
        n = 1;
    }
    printf("%d\t%s\n", s, argv[n]);
    return EXIT_SUCCESS;
}
